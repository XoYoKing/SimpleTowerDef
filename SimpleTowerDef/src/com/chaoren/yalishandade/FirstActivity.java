package com.chaoren.yalishandade;

import java.util.Timer;
import java.util.TimerTask;



import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class FirstActivity extends Activity {

	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	Button startButton;
	
	MediaPlayer mPlayer;
	
	
	
	
	
   
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int widthPixels= dm.widthPixels;
        int heightPixels= dm.heightPixels;
        
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);  
       
        mPlayer=MediaPlayer.create(this, R.raw.background);
        
        
        startButton=(Button)findViewById(R.id.buttonstart);
       
        
        preferences=getSharedPreferences("YaLiShanDa", Context.MODE_PRIVATE);
        
        
        
        GlobalObject tmpGlobalObject=(GlobalObject) getApplication();
        tmpGlobalObject.iHp=preferences.getInt("Hp", 20);
        tmpGlobalObject.iGold=preferences.getInt("Gold", 100);
        tmpGlobalObject.iPass=preferences.getInt("Pass", 2);
        
        tmpGlobalObject.iSreenHeight=heightPixels;
        tmpGlobalObject.iSreenWidth=widthPixels;
        //Toast.makeText(this, String.valueOf(widthPixels), Toast.LENGTH_LONG).show();
        try {
			mPlayer.start();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
        startButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(FirstActivity.this, TwoActivity.class);
				startActivity(intent);
				finish();
			}
		});
       
        
    
    }

    
    

    @Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mPlayer.stop();
	}




	
    
	
}
