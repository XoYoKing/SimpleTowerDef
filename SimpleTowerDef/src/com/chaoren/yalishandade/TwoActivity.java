package com.chaoren.yalishandade;



import java.util.ArrayList;



import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TwoActivity extends Activity {

	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;
	
	Button btn5;
	Button btn6;
	Button btn7;
	Button btn8;
	Button btn9;
	Button btns;
	Button btn10;
	Button btn11;
	
	
	ArrayList<Button> btnArrayList=new ArrayList<Button>();
	
	
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	TextView goldTextView;
	TextView hearTextView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_two);
		
		btn1=(Button)findViewById(R.id.button11);
		btn2=(Button)findViewById(R.id.toresume);
		btn3=(Button)findViewById(R.id.button3);
		btn4=(Button)findViewById(R.id.button4);
		btn5=(Button)findViewById(R.id.button5);
		btn6=(Button)findViewById(R.id.button6);
		btn7=(Button)findViewById(R.id.button7);
		btn8=(Button)findViewById(R.id.button8);
		btn9=(Button)findViewById(R.id.button9);
		
		btn10=(Button)findViewById(R.id.button10);
		
		
		btnArrayList.add(btn1);
		btnArrayList.add(btn2);
		btnArrayList.add(btn3);
		btnArrayList.add(btn4);
		btnArrayList.add(btn5);
		btnArrayList.add(btn6);
		btnArrayList.add(btn7);
		btnArrayList.add(btn8);
		btnArrayList.add(btn9);
		
		
		
		
		btn11=(Button)findViewById(R.id.buttongetgold);
		preferences=getSharedPreferences("YaLiShanDa", Context.MODE_PRIVATE);
		editor=preferences.edit();
		goldTextView=(TextView)findViewById(R.id.twogold);
		hearTextView=(TextView)findViewById(R.id.twoheart);
		
		
		
		
		
		
	 
	    
	    
	    
	    
		btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartGame(1);
			}
		});
		
		
		btn10.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(TwoActivity.this,ThreeActivity.class);
				startActivity(intent);
				
			}
		});
		
		btn11.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GlobalObject tmpGlobalObject=(GlobalObject)getApplication();
				
				tmpGlobalObject.iHp+=10;
				tmpGlobalObject.iGold+=30;
				goldTextView.setText(String.valueOf(tmpGlobalObject.iGold));
				hearTextView.setText(String.valueOf(tmpGlobalObject.iHp));
				Toast.makeText(getApplicationContext(), String.valueOf("看看左上角，已经涨啦~") , Toast.LENGTH_LONG).show();
								
			}
		});
btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				StartGame(2);
			}
		});
btn3.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(3);
	}
});
btn4.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(4);
	}
});
btn5.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(5);
	}
});
btn6.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(6);
	}
});
btn7.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(7);
	}
});
	btn8.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(8);
	}
	});
	btn9.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		StartGame(9);
	}
	});
	
	
	
	
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.two, menu);
		return true;
	}
	
	void StartGame(int aPass)
	{
		
		Intent intent=new Intent(this,OneActivity.class);
		intent.putExtra("pass", aPass);
		startActivity(intent);
		finish();
	}


	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		try {
			GlobalObject tmpGlobalObject=(GlobalObject)getApplication();
			editor.putInt("Hp", tmpGlobalObject.iHp);
			editor.putInt("Gold", tmpGlobalObject.iGold);
			editor.putInt("Pass",tmpGlobalObject.iPass);
			editor.commit();
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("errof", e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		super.onDestroy();
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		//finish();
		
		
		if(keyCode==KeyEvent.KEYCODE_BACK&&event.getRepeatCount()==0)
		{
			
			ShowPause();
			return false;
		}
		
		return true;
		
		
	}


	void ShowPause()
	{
		new AlertDialog.Builder(this).setTitle("退出?").setPositiveButton("退出", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		}).setNegativeButton("取消", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
			}
		}).show();
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		GlobalObject tmpGlobalObject=(GlobalObject)getApplication();
		goldTextView.setText(String.valueOf(tmpGlobalObject.iGold));
		hearTextView.setText(String.valueOf(tmpGlobalObject.iHp));
		
		GetPass(tmpGlobalObject.iPass);
		
		super.onResume();
	}


	void GetPass(int aPass)
	{
		for(int index=0;index<aPass;index++)
		{
			btnArrayList.get(index).setEnabled(true);
		}
		for(int index=aPass;index<9;index++)
		{
			btnArrayList.get(index).setEnabled(false);
		}
	}

}
