package com.chaoren.yalishandade;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.R.bool;
import android.R.integer;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.style.LeadingMarginSpan.LeadingMarginSpan2;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class DrawView extends View
{
	public TArrayList<Enemy> enemies=new TArrayList<Enemy>();
	public  TArrayList<Tower> towers=new TArrayList<Tower>();
	public ArrayList<CPos> iPosList=new ArrayList<CPos>();
	public Bitmap mBitmap;
	public Bitmap cacheBitmap=null;
	public Paint paint;
	public ArrayList<TowerLabel> iTowerLabels=new ArrayList<TowerLabel>();
	public TArrayList<Bullet> iBullets=new TArrayList<Bullet>();
	public ArrayList<CPathPoint> iCPathPoints=new ArrayList<CPathPoint>();
	
	
	public Boolean iStart=false;
	public Boolean iInNewTower=false;
	public TowerMov iTowerMov=null;
	public int iHP=1000;
	public int iGold=100;
	public int iScore=0;
	
	public Bitmap iBackGroundBitmap;
	
	public Bitmap iBackGroundBitmap2;
	
	public Bitmap iRoadBitmap;
	
	public Bitmap iGoldBitmap;
	public Bitmap iHpBitmap;
	public Bitmap iScoreBitmap;
	public Paint iTowerPaint;
	
	public Boolean iUpLevel=false;
	
	public CUpLevel iCUpLevelSprite;
	
	public final int END=2;
	public final int ON=1;
	public  final int PAUSE=3;
	public  final int INIT=0;
	public final int WIN=4;
	public int iPass=1;
	public Boolean onStartBoolean=false;
	public int GameState=INIT;
	
	public int iSpriteWidth;
	public int iSpriteHeight;
	
	public DrawView(Context context, AttributeSet set)
	{
		super(context, set);
		
		//开启定时器，然后加一个定时器的节奏
		//定时器开始之后，只要每次更新就可以了
		LoadResoures();
		paint=new Paint(Paint.DITHER_FLAG);
		paint.setColor(Color.YELLOW);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		paint.setStrokeWidth(2);
		paint.setDither(true);
		paint.setAntiAlias(true);
		paint.setTextSize(36);
		paint.setTypeface(Typeface.SANS_SERIF);
		
		iTowerPaint=new Paint(Paint.DITHER_FLAG);
		iTowerPaint.setColor(Color.RED);
		iTowerPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		iTowerPaint.setTextSize(25);
		iTowerPaint.setTypeface(Typeface.SANS_SERIF);
		

	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		float x=event.getX();	
		float y=event.getY();
		Log.i("event", event.toString());
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			
			DealTouchDownEvent(x,y);
			
		
		case MotionEvent.ACTION_MOVE:
			Log.i("move", "坐标："+String.valueOf(x)+","+String.valueOf(y));
			DealTouchMoveEvent(x, y);
			break;
			
		case MotionEvent.ACTION_UP:
			DealTouchUpEvent(x, y);
			Log.i("up", "坐标："+String.valueOf(x)+","+String.valueOf(y));
		default:
			break;
		}
		return true;
	}	
	@Override
	public void onDraw(Canvas canvas)
	{
		//画背景
		canvas.drawBitmap(iBackGroundBitmap,0,0, null);
		
		//画出Path的Sprite
		int  pathindex=0;
		for(pathindex=0;pathindex<iCPathPoints.size();pathindex++)
		{
			Sprite tmpSprite=iCPathPoints.get(pathindex);
			//canvas.drawCircle(tmpSprite.iX, tmpSprite.iY, 24, paint);
			//canvas.drawText(String.valueOf(tmpSprite.iX), tmpSprite.iX, tmpSprite.iY, paint);
			//canvas.drawBitmap(iRoadBitmap, tmpSprite.iX-tmpSprite.iWidth/2,tmpSprite.iY,paint);
		}
		
		//画出生命值
		//画出积分
		//画出金币
		canvas.drawBitmap(iGoldBitmap, 450, 10,paint);
		canvas.drawBitmap(iHpBitmap, 590, 10,paint);
		canvas.drawBitmap(iScoreBitmap, 726,10, paint);
		

		//画金币
		canvas.drawText(String.valueOf(iGold), 490, 40, paint);
		
		//画生命值
		canvas.drawText(String.valueOf(iHP), 630, 40, paint);
		
		//画积分
		canvas.drawText(String.valueOf(iScore), 766, 40, paint);
		
		
		
		
		
		
		
		
		//将所有敌人的轨迹画出
		int index=0;
		
		//画敌人的轨迹
		Enemy emEnemy;
		for(index=0;index<enemies.size();index++)
		{
			emEnemy=enemies.get(index);
			Log.i("enemy", "坐标:"+String.valueOf(emEnemy.iX)+","+String.valueOf(emEnemy.iY));
			//canvas.drawCircle(emEnemy.iX, emEnemy.iY, 10,paint);
			emEnemy.Draw(canvas);
		}
		
		//那些不动的塔标
		
		TowerLabel towerLabel;
		for(index=0;index<iTowerLabels.size();index++)
		{
			towerLabel=iTowerLabels.get(index);
			//canvas.drawCircle(towerLabel.iX, towerLabel.iY, 30, towerLabel.iPaint);
			towerLabel.Draw(canvas,iGold);
		}
		
		
		//判断是否在建造，如果是在建造的话，画出手拖动的那个图
		if(iInNewTower==true&&iTowerMov!=null)
		{
			
			
			iTowerMov.Draw(canvas);
			iTowerMov.DrawCircle(canvas, iCPathPoints, towers);
			
			
		}
		
		
		//塔
		for (index=0;index<towers.size();index++)
		{
			towers.get(index).draw(canvas,iTowerPaint);
		}
		
		
		//子弹
		for(index=0;index<iBullets.size();index++)
		{
			iBullets.get(index).Draw(canvas);
		}
			
		
		//画出是否升级或出售
		if(iUpLevel==true)
		{
			iCUpLevelSprite.Draw(canvas, paint);
		}
		
		
		
		
		
	}
	
	
	public void LoadResoures()
	{
		
		InputStream is = getResources().openRawResource(R.drawable.soundon);    
        mBitmap = BitmapFactory.decodeStream(is);   
		
	}
	
	public Boolean InWarArea(Sprite aSprite)
	{
		if(aSprite.iY>100)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	
	//现在一切都以中心坐标为中心
	public int TouchTowerLabel(float posX,float posY)
	{
		//检测是否碰到TowerLabel
		//返回TowerLabel的index
		int num=iTowerLabels.size();
		int index=0;
		int DestX=100;
		int DestY=100;
		TowerLabel towerLabel;
		for(index=0;index<num;index++)
		{
			towerLabel=iTowerLabels.get(index);
			DestX=(int) Math.abs(towerLabel.iX-posX);
			DestY=(int) Math.abs(towerLabel.iY-posY);
			Log.i("TouchTowerLabel", "posX:"+String.valueOf(posX)+",posY:"+String.valueOf(posY));
			Log.i("TouchTowerLabel", "DestX:"+String.valueOf(DestX)+",DestY:"+String.valueOf(DestY));
			if(DestX<towerLabel.iWidth&&DestY<towerLabel.iHeight&&iGold>=towerLabel.iCost)
			{
				Log.i("TouchTowerLabel", "触碰："+String.valueOf(index));
				
				return index;
				
			}
		}
		Log.i("TouchTowerLabel", "没有触碰："+String.valueOf(index));
		return -1;
	}
	
	/**
	 * 获取在哪个区域
	 * @param posX
	 * @param posY
	 * @return
	 */
	public int GetInTouchArea(float aPosX,float aPosY)
	{
		if(aPosX<400&&aPosY<80)
		{
			Log.i("TouchTowerLabel","0区域:"+String.valueOf(aPosX)+","+String.valueOf(aPosY));
			return 0;
		}	
		else if(aPosX>400&&aPosY<80)
		{
			Log.i("TouchTowerLabel","1区域:"+String.valueOf(aPosX)+","+String.valueOf(aPosY));
			return 1;
		}
		
			
		else {
			Log.i("TouchTowerLabel","2区域:"+String.valueOf(aPosX)+","+String.valueOf(aPosY));
			return 2;
		}
		
	}
	
	public void DealTouchDownEvent(float aPosX,float aPosY)
	{
		iInNewTower=false;
		switch (GetInTouchArea(aPosX, aPosY)) {
		//0表示选中那些那些区域
		case 0:
		{
			int TouchIndex=TouchTowerLabel(aPosX, aPosY);
			if(TouchIndex==-1)
			{
				
				break;
			}
			else 
			{
				iInNewTower=true;
				iTowerMov=new TowerMov(iTowerLabels.get(TouchIndex));
			}
			break;
		}
		
		
		case 2:
		{
			if(iUpLevel==false)
			{
				HandSprite tmpSprite=new HandSprite((int)aPosX,(int)aPosY);
				
				int index=GetTouchTowerIndex(tmpSprite);
				if(index!=-1)
				{
					iUpLevel=true;
					iCUpLevelSprite.SetTower(towers.get(index));
				}
			}
			else if(iUpLevel==true)
			{
				HandSprite tmpSprite=new HandSprite((int)aPosX,(int)aPosY);
				
				switch (iCUpLevelSprite.GetSelect(tmpSprite)) {
				//如果可以升级
				case 0:
					if(iCUpLevelSprite.iTowerSprite!=null)
					{
						if(iGold>=iCUpLevelSprite.iTowerSprite.iCost)
						{
							iGold-=iCUpLevelSprite.iTowerSprite.iCost;
							iCUpLevelSprite.iTowerSprite.UpLevel();
							iCUpLevelSprite.iToastString="";
							iUpLevel=false;
						}
						else
						{
							iCUpLevelSprite.iToastString="金币不足";
						}
						
					}
					break;
				
				case 1:
					if(iCUpLevelSprite.iTowerSprite!=null)
					{
						iGold+=iCUpLevelSprite.iTowerSprite.iCost/2;
						towers.remove(iCUpLevelSprite.iTowerSprite);
						iCUpLevelSprite.iTowerSprite=null;
						iUpLevel=false;
					}
					break;
				case 2:
					iUpLevel=false;
					break;
					
					
				default:
					break;
				}
				
				
			}
			
			break;
		}
			
		
		
		default:
			break;
		}
		
	}
	
	public void DealTouchMoveEvent(float aPosX,float aPosY)
	{
		if(iInNewTower==true&iTowerMov!=null)
		{
			iTowerMov.iX=(int) aPosX;
			iTowerMov.iY=(int) aPosY;
		}
	}
	
	public void DealTouchUpEvent(float aPosX,float aPosY)
	{
		if(iInNewTower==true)
		{
			//开始建造
			if(iTowerMov.iCanBuild==true&&GetInTouchArea(aPosX,aPosY)==2)
			{
				iGold-=iTowerMov.iTowerLabel.iCost;
				towers.add(new Tower(iTowerMov.iTowerLabel.iType, (int)aPosX, (int)aPosY, enemies,iBullets,iTowerMov.iTowerLabel.iCost,iSpriteWidth,iSpriteHeight));
			}
			
		}
		iInNewTower=false;
		//这里需要判断是否在可建造区域内，然后进行处理	
	}
	
	
	public int GetTouchTowerIndex(Sprite aSprite)
	{
		for (int index=0;index<towers.size();index++)
		{
			if(aSprite.DetectCrash(towers.get(index)))
			{
				return index;
			}
		}
		return -1;
	}
	
}
