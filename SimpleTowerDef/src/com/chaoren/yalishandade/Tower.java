package com.chaoren.yalishandade;

import java.io.Serializable;
import java.util.ArrayList;

import android.R.integer;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Tower extends Sprite implements Serializable{
	//塔的设计
	//点击地面，如果是处于当中的情况中，则可以建立塔
	//还需考虑射程
	//塔的类型决定了它是否能够发送什么类型的子弹
	//同时，塔的类型也决定了它的射程，
	//之后的设计可以是，升级塔可以升级它的攻击力以及射程
	public int fps=10;//帧数
	public int iType;
	public int iAttackRange;//
	int iRanges[]={100,120,140,160,180};
	public  int iAttackSpeed;//攻击速度，10表示1秒发生一次子弹，20表示2秒发射一次子弹
	public int iAttackSpeeds[]={10,10,15,20,25};
	public int iTimeStep=0; //判断什么时候开始进行攻击
	public TArrayList<Enemy> iEnemies;
	public TArrayList<Bullet> iBullets;
	public int iLevel=1;
	public int iCost=10;
	
	//构造函数
	public Tower (int aType,int aX,int aY,TArrayList<Enemy> aEnemies,TArrayList<Bullet> aBullets,int aCost,int aWidth,int aHeight)
	{
		iType=aType;
		iAttackRange=iRanges[aType];
		iAttackSpeed=iAttackSpeeds[aType];
		iX=aX;
		iY=aY;
		iEnemies=aEnemies;
		iBullets=aBullets;
		iCost=aCost;
		iHeight=aHeight;
		iWidth=aWidth;
	}
	
	//判断是否进行攻击的对象，然后返回攻击的对象index
	
	public int AttackIndex()
	{
		//判断距离
		iTimeStep++;
		if(iTimeStep%iAttackSpeed==0)
		{
			int index=0;
			for (;index<iEnemies.size();index++)
			{
				if(GetDest(iEnemies.get(index))<iAttackRange)
				{
					
					return index;
				}
			}
		}
		return -1;
	}
	// 发送子弹
	public void iSendBullet()
	{
		int attackindex=AttackIndex();
		if(attackindex!=-1)
		{
			iBullets.add(new Bullet(iType, iEnemies.get(attackindex),iX,iY,iLevel));
		}
	}
	
	
	//每个对象对自己的画图进行负责
	
	public void draw(Canvas canvas, Paint aPaint)
	{
		canvas.drawBitmap(ResBitmaps.getResBitmaps().iTowerBitmaps.get(iType).towerBitmaps.get(0),iX-iWidth/2 ,iY-iHeight/2,null);
		canvas.drawText(String.valueOf(iLevel), iX+iWidth/2,iY, aPaint);
		
	}
	
	public void UpLevel()
	{
		iLevel+=1;
		iAttackRange+=5;
		
	}
	
}
