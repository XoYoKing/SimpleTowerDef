package com.chaoren.yalishandade;

import java.util.ArrayList;

import android.R.integer;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;

public class ResBitmaps {
	//这个类是管理所有精灵的图像的类，不包括背景，图标等图片
	//这个类是创建所有的图片的，
	//但是仍然需要画很多的图片 
	
	 private static ResBitmaps iResBitmaps;
	 ArrayList<EnemyBitmaps> iEnemyBitmaps=new ArrayList<EnemyBitmaps>();
	 ArrayList<TowerBitmaps> iTowerBitmaps=new ArrayList<TowerBitmaps>();
	 ArrayList<TowerLabelBitmaps> iTowerLabelBitmaps=new ArrayList<TowerLabelBitmaps>();
	 ArrayList<BulletBitmaps> iBulletBitmaps=new ArrayList<BulletBitmaps>();
	
	 
	 Bitmap Resize(int aWidth,int aHeight,Bitmap aBitmap)
	 {
		  float scaleWidth = ((float) aWidth) / aBitmap.getWidth();
		  float scaleHeight = ((float) aHeight) / aBitmap.getHeight();
		  Matrix matrix = new Matrix();
		  matrix.postScale(scaleWidth, scaleHeight);
		  
		  return Bitmap.createBitmap(aBitmap, 0, 0, aBitmap.getWidth(), aBitmap.getHeight(), matrix, true);
		 
	 }
	 
	 
	
	 public void Recy()
	 {
		 
		 for(int index=iEnemyBitmaps.size()-1;index>=0;index--)
		 {
			 iEnemyBitmaps.get(index).recy();
		 }
		 
		 for(int index=iTowerBitmaps.size()-1;index>=0;index--)
		 {
			 iTowerBitmaps.get(index).Recy();
		 }
		 for(int index=iTowerLabelBitmaps.size()-1;index>=0;index--)
		 {
			 iTowerLabelBitmaps.get(index).Recy();
		 }
		 for(int index=iBulletBitmaps.size()-1;index>=0;index--)
		 {
			 iBulletBitmaps.get(index).Recy();
		 }
		 iResBitmaps=null;
	 }
	 
	 
	 
	public static ResBitmaps getResBitmaps()
	{
		return iResBitmaps;
	}
	 
	public static ResBitmaps InitResBitmaps(Resources aRes,int aPass,int aWidth,int aHeight)
	{
		if(iResBitmaps==null)
		{
			iResBitmaps=new ResBitmaps(aRes,aPass,aWidth,aHeight);
		}
		return iResBitmaps;
	}
	
	
	
	//这里是添加图片的时候
	private ResBitmaps(Resources aRes,int aPass,int aWidth,int aHeight)
	{
		//这里是将所有图片添加进去的东西
		//先添加一个塔，一个敌人，一个子弹，一个塔标
		
		switch (aPass) {
		case 1:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en1, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en11, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en21, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en31, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en41, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en51, 3,aWidth,aHeight));
			break;
		case 2:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en2, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en12, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en22, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en32, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en42, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en52, 3,aWidth,aHeight));
			break;
		case 3:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en3, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en13, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en23, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en33, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en43, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en53, 3,aWidth,aHeight));
			
			break;
		case 4:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en4, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en14, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en24, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en34, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en44, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en54, 3,aWidth,aHeight));
			break;
		case 5:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en5, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en15, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en25, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en35, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en45, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en55, 3,aWidth,aHeight));
			break;
		case 6:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en6, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en16, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en26, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en36, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en46, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en56, 3,aWidth,aHeight));
			break;
		case 7:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en7, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en17, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en27, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en37, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en47, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en57, 3,aWidth,aHeight));
			break;
			
		case 8:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en8, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en18, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en28, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en38, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en48, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en58, 3,aWidth,aHeight));
			break;
		case 9:
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en8, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en18, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en28, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en38, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en48, 3,aWidth,aHeight));
			iEnemyBitmaps.add(new EnemyBitmaps(aRes, R.drawable.en58, 3,aWidth,aHeight));
			break;	
		default:
			break;
		}
		

		
		iTowerBitmaps.add(new TowerBitmaps(aRes, R.drawable.tower1, 1,aWidth,aHeight));
		iTowerBitmaps.add(new TowerBitmaps(aRes, R.drawable.tower2,1,aWidth,aHeight));
		iTowerBitmaps.add(new TowerBitmaps(aRes, R.drawable.tower3, 1,aWidth,aHeight));
		iTowerBitmaps.add(new TowerBitmaps(aRes, R.drawable.tower4, 1,aWidth,aHeight));
		iTowerBitmaps.add(new TowerBitmaps(aRes, R.drawable.tower5, 1,aWidth,aHeight));
		
		
		
		
		iBulletBitmaps.add(new BulletBitmaps(aRes, R.drawable.bl1, 1));
		iBulletBitmaps.add(new BulletBitmaps(aRes, R.drawable.bl2, 1));
		iBulletBitmaps.add(new BulletBitmaps(aRes, R.drawable.bl3, 1));
		iBulletBitmaps.add(new BulletBitmaps(aRes, R.drawable.bl4, 1));
		iBulletBitmaps.add(new BulletBitmaps(aRes, R.drawable.bl5, 1));
		
		
		iTowerLabelBitmaps.add(new TowerLabelBitmaps(aRes, R.drawable.tower1,1,aWidth,aHeight));
		iTowerLabelBitmaps.add(new TowerLabelBitmaps(aRes, R.drawable.tower2,1,aWidth,aHeight));
		iTowerLabelBitmaps.add(new TowerLabelBitmaps(aRes, R.drawable.tower3,1,aWidth,aHeight));
		iTowerLabelBitmaps.add(new TowerLabelBitmaps(aRes, R.drawable.tower4,1,aWidth,aHeight));
		iTowerLabelBitmaps.add(new TowerLabelBitmaps(aRes, R.drawable.tower5,1,aWidth,aHeight));
	}
	
	//图片应该是有两种状态的，其余的图片继承这个，然后添加新的图片
	
	
	
	//敌人图片
	class EnemyBitmaps 
	{
		//敌人的图片有四种状态
		//左走，有走，上走，下走，如果消失的话，那么就消失
		public ArrayList<Bitmap> upBitmaps=new ArrayList<Bitmap>();
		public  ArrayList<Bitmap> downBitmaps=new ArrayList<Bitmap>();
		public  ArrayList<Bitmap> leftBitmaps=new ArrayList<Bitmap>();
		public ArrayList<Bitmap> rightBitmaps=new ArrayList<Bitmap>();
		
		public void recy()
		{
			for(int index=upBitmaps.size()-1;index>=0;index--)
			{
				upBitmaps.get(index).recycle();
				downBitmaps.get(index).recycle();
				leftBitmaps.get(index).recycle();
				rightBitmaps.get(index).recycle();
			}
			upBitmaps=null;
			downBitmaps=null;
			leftBitmaps=null;
			rightBitmaps=null;
		}
		
		
		
		
		 
		
		
		
		public EnemyBitmaps(Resources res,int aDrawableId,int aCol,int aWidth,int aHeight)
		{
			
			Bitmap bitResBitmap = BitmapFactory.decodeResource(res, aDrawableId);
			Log.i("bitmap","图片大小:"+String.valueOf(bitResBitmap.getWidth())+","+String.valueOf(bitResBitmap.getHeight()));
			Bitmap up;
			Bitmap down;
			Bitmap left;
			Bitmap right;
			int width=bitResBitmap.getWidth()/aCol;
			int height=bitResBitmap.getHeight()/4;
			
			try {
				for(int index=0;index<aCol;index++)
				{
					
					down=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap, index*width, 0,width, height));			
					left=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap, index*width,height, width, height));
					
					right=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap,index*width,2*height,width,height));
					
					up=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap,index*width,3*height,width,height));
					upBitmaps.add(up);
					downBitmaps.add(down);
					leftBitmaps.add(left);
					rightBitmaps.add(right);
				}
			} catch (Exception e) {
				// TODO: handle exception
				Log.i("bitmap", e.getMessage());
			}
			
		}	
	}
	
	//塔之图片
	//塔的图片则有八个方位，所以需要八个不同的角度，但是也可以只有一张图片,所以这个还是设置成一张图片
	class TowerBitmaps 
	{
		ArrayList<Bitmap> towerBitmaps=new ArrayList<Bitmap>();
		public TowerBitmaps(Resources res, int aDrawableId, int aCol,int aWidth,int aHeight)
		{
			Bitmap bitResBitmap = BitmapFactory.decodeResource(res, aDrawableId);
			Bitmap tmpBitmap;
			int width=bitResBitmap.getWidth()/aCol;
			int height=bitResBitmap.getHeight()/2;
			for(int index=0;index<aCol;index++)
			{
				tmpBitmap=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap,index*width,0,width,height));
				towerBitmaps.add(tmpBitmap);
			}
			
			
		}
		void Recy()
		{
			for(int index=towerBitmaps.size()-1;index>=0;index--)
			{
				towerBitmaps.get(index).recycle();
			}
		}
		
		
		
		
		
	}
	
	class TowerLabelBitmaps 
	{
		ArrayList<Bitmap> towerBitmaps=new ArrayList<Bitmap>();
		ArrayList< Bitmap> notowerBitmaps=new ArrayList<Bitmap>();
		public TowerLabelBitmaps(Resources res, int aDrawableId, int aCol,int aWidth,int aHeight) {
			
			// TODO Auto-generated constructor stub
			Bitmap bitResBitmap = BitmapFactory.decodeResource(res, aDrawableId);
			Bitmap tmpBitmap;
			int width=bitResBitmap.getWidth()/aCol;
			int height=bitResBitmap.getHeight()/2;
			for(int index=0;index<aCol;index++)
			{
				tmpBitmap=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap,index*width,0,width,height));
				towerBitmaps.add(tmpBitmap);
				tmpBitmap=Resize(aWidth, aHeight, Bitmap.createBitmap(bitResBitmap,index*width,height,width,height));
				notowerBitmaps.add(tmpBitmap);
			}
		}
		
		
		void Recy()
		{
			for(int index=towerBitmaps.size()-1;index>=0;index--)
			{
				towerBitmaps.get(index).recycle();
			}
			towerBitmaps=null;
			for(int index=notowerBitmaps.size()-1;index>=0;index--)
			{
				notowerBitmaps.get(index).recycle();
			}
			notowerBitmaps=null;
		}
		
		
		
		
		
		
	}
	
	//子弹有两种状态，一种是子弹作用效果，另外一种是就是飞行效果，除了这两个之外没有了
	class BulletBitmaps 
	{
		
		public ArrayList<Bitmap> norBitmaps=new ArrayList<Bitmap>();
		public ArrayList<Bitmap> effectBitmaps=new ArrayList<Bitmap>();
		public BulletBitmaps(Resources res, int aDrawableId, int aCol) {
			// TODO Auto-generated constructor stub
			Bitmap bitResBitmap = BitmapFactory.decodeResource(res, aDrawableId);
			Bitmap tmpnorBitmap;
			Bitmap tmpeffectBitmap;
			int width=bitResBitmap.getWidth()/aCol;
			int height=bitResBitmap.getHeight()/2;
			for (int index=0;index<aCol;index++)
			{
				tmpnorBitmap=Bitmap.createBitmap(bitResBitmap,index*width,0,width,height);
				tmpeffectBitmap=Bitmap.createBitmap(bitResBitmap,index*width,height,width,height);
				norBitmaps.add(tmpnorBitmap);
				effectBitmaps.add(tmpeffectBitmap);
			}
		
		}
		
		
		public void Recy()
		{
			for(int index=norBitmaps.size()-1;index>=0;index--)
			{
				norBitmaps.get(index).recycle();
			}
			norBitmaps=null;
			for (int index=effectBitmaps.size()-1;index>=0;index--)
			{
				effectBitmaps.get(index).recycle();
			}
			effectBitmaps=null;
			
		}
		
	}
	
}
