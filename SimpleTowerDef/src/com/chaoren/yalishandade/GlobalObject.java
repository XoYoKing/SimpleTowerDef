package com.chaoren.yalishandade;

import java.util.ArrayList;

import android.R.integer;
import android.app.Application;

public class GlobalObject extends Application{
	public int iSreenWidth=800;
	public int iSreenHeight=480;
	public int iHp=40;
	public int iScore=0;
	public int iGold=100;
	public int iPass=1;
	public TArrayList<Enemy> enemies=new TArrayList<Enemy>();
	public TArrayList<Tower> towers=new TArrayList<Tower>();
	public TArrayList<Bullet> bullets=new TArrayList<Bullet>();
}
