package com.chaoren.yalishandade;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.R.integer;
import android.os.Environment;

public class EnemyPath {
	ArrayList<MPosition> iPath=new ArrayList<MPosition>();
	public EnemyPath()
	{
		read();
	}
	String FILE_NAME="file:///android_assets/PATH.TXT";
	private void read() {
		try {
		
			if (Environment.getExternalStorageState().equals(
					Environment.MEDIA_MOUNTED)) {
				
				File sdCardDir = Environment.getExternalStorageDirectory();
				
				FileInputStream fis = new FileInputStream(sdCardDir
						.getCanonicalPath()
						+ FILE_NAME);
			
				BufferedReader br = new BufferedReader(new InputStreamReader(
						fis));
				StringBuilder sb = new StringBuilder("");
				String line = null;
				while ((line = br.readLine()) != null) {
					//sb.append(line);
					String[] XY=line.split(",");
					int X=Integer.parseInt(XY[0]);
					int Y=Integer.parseInt(XY[1]);
					iPath.add(new MPosition(X,Y));
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
class MPosition
{
	public MPosition(int aX,int aY)
	{
		iX=aX;
		iY=aY;
	}
	public int iX;
	public  int iY;
	
}
