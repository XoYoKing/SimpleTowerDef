package com.chaoren.yalishandade;

import java.io.Serializable;
import java.util.ArrayList;

import android.R.integer;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class Enemy extends Sprite implements Serializable{
	public float iSpeed;
	public int iPathIndex;
	public ArrayList<CPos> iPosList;
	public int iHp=10;
	public int iEnmyTimeStep=0;
	public TPaint iPaint;
	public int iWard=0;
	public float iFullHp=10;
	
	//敌人的路线画图还有需要时
	public Enemy(float aSpeed ,ArrayList<CPos> aPosList,int aLabel,TPaint aPaint,int aWard,int aHp,int aWidth,int aHeight)
	{
		iSpeed=aSpeed;
		iPathIndex=0;
		iPosList=aPosList;
		iX=aPosList.get(0).iX;
		iY=aPosList.get(0).iY;
		iBitmapLabel=aLabel;
		iPaint=aPaint;
		iWard=aWard;
		iHp=aHp;
		iFullHp=iHp;
	
	}
	
	
	//敌人要根据它的行踪进行画图
	public void Draw( Canvas canvas)
	{
		//对于Hp问题，既然敌人没有消失，那么就应该继续画，至于是否应该消失的问题，留给主要逻辑判断
		//Log.i("Hp", "敌人Hp:"+String.valueOf(iHp));
		//判断当前的行进路线，根据行进路线判断是否进行处理
		iEnmyTimeStep++;
		try {
			if(iPathIndex<iPosList.size()-1)
			{
				int SrcX=iPosList.get(iPathIndex).iX;
				int SrcY=iPosList.get(iPathIndex).iY;
				int DestX=iPosList.get(iPathIndex+1).iX;
				int DestY=iPosList.get(iPathIndex+1).iY;
			
				
				Bitmap tmpBitmap;
				//Log.i("dir","敌人源坐标:"+String.valueOf(SrcX)+","+String.valueOf(SrcY)+";目的坐标:"+String.valueOf(DestX)+","+String.valueOf(DestY));
				//右
				if(DestX>SrcX&&DestY==SrcY)
				{
					canvas.drawBitmap(ResBitmaps.getResBitmaps().iEnemyBitmaps.get(iBitmapLabel).rightBitmaps.get(iEnmyTimeStep%3),iX-iWidth/2, iY-iHeight/2, null)	;
				}
				//左
				else if(DestX<SrcX&&DestY==SrcY) 
				{
					canvas.drawBitmap(ResBitmaps.getResBitmaps().iEnemyBitmaps.get(iBitmapLabel).leftBitmaps.get(iEnmyTimeStep%3),iX-iWidth/2, iY-iHeight/2, null)	;
				}
				//上
				else if(DestX==SrcX&&DestY<SrcY)
				{
					canvas.drawBitmap(ResBitmaps.getResBitmaps().iEnemyBitmaps.get(iBitmapLabel).upBitmaps.get(iEnmyTimeStep%3),iX-iWidth/2, iY-iHeight/2, null)	;
				}
				//下
				else if(DestX==SrcX&&DestY>SrcY)
				{
					canvas.drawBitmap(ResBitmaps.getResBitmaps().iEnemyBitmaps.get(iBitmapLabel).downBitmaps.get(iEnmyTimeStep%3),iX-iWidth/2, iY-iHeight/2, null)	;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("Hp", e.getMessage());
		}
		//另外，敌人还需要画一条血表示
		canvas.drawLine(iX-iWidth/2,iY-iHeight/2, iX+iHp/iFullHp*iWidth, iY-iHeight/2, iPaint);
		
		
		
	}

}