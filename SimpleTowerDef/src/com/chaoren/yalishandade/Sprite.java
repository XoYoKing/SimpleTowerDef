package com.chaoren.yalishandade;


import android.util.Log;

public class Sprite {
	public int fps;
	//各个类的画图还是由自己画出
	//每个类还需要为自己贴上标签，属于第几类敌人
	public int iWidth=48;
	public int iHeight=48;
	public int iStatus=0;
	public int iX=240;
	public int iY=599;
	public  int iDisplayGap=1;
	public int iImgStatus=0;
	//如果即将消失，那么还需要一个消失的过程，这个初始值为-1 
	//如果即将消失，那么改变其中的消亡状态，这个应该如何运用呢，如果达到消亡时间，那么就不会画这个东西
	//目前可能用到的是子弹
	//但是，将其从画布从取消，还是要在这个逻辑中判断消除
	//通过这个消亡时间，可以对其中的减速效果进行延时以及减速进行处理
	 public int iOverTime=-1; 
	 public int iBitmapLabel=-1;
	 public Boolean DetectCrash(Sprite aSprite)
	 {
		 int DestX=Math.abs(iX-aSprite.iX);
		 int DestY=Math.abs(iY-aSprite.iY);
		 if(DestX<(iWidth/2+aSprite.iWidth/2)&&DestY<(iHeight/2+aSprite.iHeight/2))
		 {
			 //Log.i("crash","碰撞：DestX:"+String.valueOf(DestX)+",DestY:"+String.valueOf(DestY));
			 return true;
		 }
		 else 
		 {
			 //Log.i("crash","没有碰撞：DestX:"+String.valueOf(DestX)+",DestY:"+String.valueOf(DestY));
			 return false;
		 }	 
	 }
	 
	 //返回两者距离
	public int GetDest(Sprite aSprite)
	{
		 int DestX=Math.abs(iX-aSprite.iX);
		 int DestY=Math.abs(iY-aSprite.iY);
		 return (int) Math.sqrt(DestX*DestX+DestY*DestY);
	}
	
	
	
	 
	 
	
	
	
	

}
