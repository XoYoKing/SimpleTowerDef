package com.chaoren.yalishandade;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class CUpLevel {
	Sprite upLevelSprite;
	Sprite sellSprite;
	Bitmap updateBitmap;
	Bitmap sellBitmap;
	Tower iTowerSprite;
	String iToastString="";
	
	public CUpLevel(Resources aResources,int asellRid,int aUpdateRid)
	{
		sellBitmap=BitmapFactory.decodeResource(aResources, asellRid);
		updateBitmap=BitmapFactory.decodeResource(aResources, aUpdateRid);
		upLevelSprite=new Sprite();
		sellSprite=new Sprite();
	}
	
	public void Draw(Canvas canvas,Paint aPaint)
	{
		//canvas.drawCircle(upLevelSprite.iX, upLevelSprite.iY, upLevelSprite.iWidth/2,aPaint);
		canvas.drawBitmap(updateBitmap	, upLevelSprite.iX-upLevelSprite.iWidth/2,upLevelSprite.iY-upLevelSprite.iHeight/2, null);
		canvas.drawBitmap(sellBitmap, sellSprite.iX-sellSprite.iWidth/2,sellSprite.iY-sellSprite.iHeight/2, null);
		canvas.drawText(iToastString, sellSprite.iX+sellSprite.iWidth, sellSprite.iY+sellSprite.iHeight, aPaint);
	}
	
	public void SetTower(Tower aTower)
	{
		iTowerSprite=aTower;
		upLevelSprite.iX=iTowerSprite.iX+iTowerSprite.iWidth;
		upLevelSprite.iY=iTowerSprite.iY;
		sellSprite.iX=iTowerSprite.iX+iTowerSprite.iWidth*3;
		sellSprite.iY=iTowerSprite.iY;
		iToastString="";
	}
	
	public int GetSelect(Sprite aSprite)
	{
		if(upLevelSprite.DetectCrash(aSprite))
		{
			return 0;
		}
		else if(sellSprite.DetectCrash(aSprite))
		{
			return 1;
		}
		else {
			return 2;
		}
		
		
		
	}

}
