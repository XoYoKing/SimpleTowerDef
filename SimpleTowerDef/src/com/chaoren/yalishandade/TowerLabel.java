package com.chaoren.yalishandade;

import android.R.integer;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

public class TowerLabel extends Sprite{
	
	public String iName;
	public Paint iPaint;
	public int iDrawStep=0;
	public int iType=0;
	public int iCost=0;
	
	public TowerLabel(String aName,int aX,int aY,int aColor,int aType,int aCost ,int aWidth,int aHeight)
	{
		iName=aName;
		iX=aX;
		iY=aY;
		iHeight=aHeight;
		iWidth=aWidth;
		iPaint=new Paint(Paint.DITHER_FLAG);
		iPaint.setColor(aColor);
		iPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		iPaint.setStrokeWidth(2);
		iPaint.setDither(true);
		iPaint.setAntiAlias(true);
		iType=aType;
		iCost=aCost;
	}
	
	
	
	public void Draw(Canvas canvas,int aGold)
	{
		iDrawStep++;
		//Log.i("Hp", "画");
		if(aGold>=iCost)
		{
			canvas.drawBitmap(ResBitmaps.getResBitmaps().iTowerLabelBitmaps.get(iType).towerBitmaps.get(iDrawStep%1), iX-iWidth/2,iY-iHeight/2, null);
		}
		else
		{
			canvas.drawBitmap(ResBitmaps.getResBitmaps().iTowerLabelBitmaps.get(iType).notowerBitmaps.get(iDrawStep%1), iX-iWidth/2,iY-iHeight/2, null);	
		}
		//Log.i("Hp", "画了");
		
		
		
	}
	
	
}
