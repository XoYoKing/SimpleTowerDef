package com.chaoren.yalishandade;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

public class TowerMov  extends Sprite{
	public int iDrawStep=0;
	public TowerLabel iTowerLabel;
	public Boolean iCanBuild=false;
	public Paint iPaint;
	
	public TowerMov(TowerLabel aTowerLabel) {
		// TODO Auto-generated constructor stub
		iTowerLabel=aTowerLabel;
		iX=aTowerLabel.iX;
		iY=aTowerLabel.iY;
		iPaint=new Paint(Paint.DITHER_FLAG);
		iPaint.setColor(Color.RED);
		iPaint.setStyle(Paint.Style.STROKE);
		iPaint.setStrokeWidth(3);
		iPaint.setDither(true);
		iPaint.setAntiAlias(true);
	}
	public void Draw(Canvas canvas)
	{
		canvas.drawBitmap(ResBitmaps.getResBitmaps().iTowerLabelBitmaps.get(iTowerLabel.iType).towerBitmaps.get(iDrawStep%1), iX-iTowerLabel.iWidth/2,iY-iTowerLabel.iHeight/2, null);
	}
	
	
	public void DrawCircle(Canvas canvas ,ArrayList<CPathPoint> aCPathPoints,ArrayList<Tower> aTowers)
	{
		Boolean isCrash=false;
		Boolean isOverLoad=false;
		for (int index=0;index<aCPathPoints.size();index++)
		{
			if(DetectCrash(aCPathPoints.get(index)))
			{
				isCrash=true;
				break;
			}
		}
		//Log.i("towers","size:"+aTowers.size());
		for(int index=0;index<aTowers.size();index++)
		{
			//Log.i("towers","tower:"+String.valueOf(aTowers.get(index).iX)+","+String.valueOf(aTowers.get(index).iY));
			//Log.i("towers", "mov:"+String.valueOf(iX)+","+String.valueOf(iY));
			
			if(DetectCrash(aTowers.get(index)))
			{
				//Log.i("towers", "触碰");
				isOverLoad=true;
				break;
			}
		}
		
		if(isCrash==false&&isOverLoad==false)
		{
			iCanBuild=true;
			iPaint.setColor(Color.GREEN);
			canvas.drawCircle(iX, iY, 48, iPaint);
		}
		else {
			iCanBuild=false;
			iPaint.setColor(Color.RED);
			canvas.drawCircle(iX, iY, 30, iPaint);
		}
		
		
		
	}
}
