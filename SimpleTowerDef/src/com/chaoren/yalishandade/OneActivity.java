package com.chaoren.yalishandade;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


import com.chaoren.yalishandade.R.id;
//import com.newqm.sdkoffer.QuMiConnect;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.text.StaticLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Toast;



public class OneActivity extends Activity {

	//如果不考虑素材的话，那么现在就可以进入主要逻辑了
	//另外要处理的问题就是屏幕分辨率的问题
	//还有保存画布的问题
	//
	Handler iHandler;
	//Boolean iStart=true;
	
	
	SharedPreferences preferences;
	SharedPreferences.Editor editor;
	
	
	MediaPlayer mPlayer;
	DrawView iDrawView;
	int iTimeTicker=0;
	int iTotalEnemySize=100;
	
	int iTimeGap=1200;
	TPaint iPaint;
	int iPass=1;
	int fps=10;
	
	int iWidth;
	int iHeight;
	GlobalObject globalObject;
	
	AlertDialog.Builder iBuilder;
	final String FILE_PATH="/YALISHANDA/history.bin";
	final String FOLDER_PATH="/YALISHANDA/";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_onee);
		 globalObject=(GlobalObject) getApplication();
		iWidth=globalObject.iSreenWidth;
		iHeight=globalObject.iSreenHeight;
		
		preferences=getSharedPreferences("YaLiShanDa", Context.MODE_PRIVATE);
		editor=preferences.edit();
		
		mPlayer=MediaPlayer.create(this, R.raw.background);
		mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				// TODO Auto-generated method stub
				try {
					mPlayer.start();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		
		
		iDrawView=(DrawView)findViewById(R.id.drawView1);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);   
		Intent tmpIntent=getIntent();
		iPass=tmpIntent.getIntExtra("pass", 1);
		Init();
		
	
		  try {
				mPlayer.start();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		
		iHandler=new Handler()
		{
			@Override
			public void handleMessage(Message message)
			{
				if(message.what==1&&iDrawView.GameState==iDrawView.ON)
				{
					UpdateMain();
				}
			}
		};
		//QuMiConnect.getQumiConnectInstance(this).initPopAd(this);
	}

	//这里才是控制的主要地方，然后画图只需要负责将每个对象的当前图片画出来即可
	void Init()
	{
		
		
		iTimeTicker=0;
		//添加敌人
		iDrawView.GameState=iDrawView.ON;
		iDrawView.iStart=false;
		iDrawView.enemies=new TArrayList<Enemy>();
		//iDrawView.enemies.add(new Enemy(23));
		iDrawView.towers=new TArrayList<Tower>();
		iDrawView.iTowerLabels=new ArrayList<TowerLabel>();
		//判断是否需要从GlobalObj获取
		GlobalObject tmpGlobalObject=(GlobalObject)getApplication();
		if(tmpGlobalObject.iHp==0)
		{
			iDrawView.iGold=100;
			iDrawView.iScore=0;
			iDrawView.iHP=40;
		}
		else
		{
			iDrawView.iGold=tmpGlobalObject.iGold;
			iDrawView.iHP=tmpGlobalObject.iHp;
			iDrawView.iScore=tmpGlobalObject.iScore;
		}
		iDrawView.iSpriteWidth=iWidth/18;
		iDrawView.iSpriteHeight=iHeight/10;

		
		iDrawView.iTowerMov=null;
		iDrawView.iInNewTower=false;
		iDrawView.iCUpLevelSprite=new CUpLevel(getResources(), R.drawable.sell,R.drawable.update);
		iDrawView.iPass=iPass;
		GetBackGround();
		GetRoad();
		
		CreatePath();
		CreatePathPoint();
		iPaint=new TPaint();
		iPaint.setColor(Color.GREEN);
		iPaint.setStyle(Paint.Style.STROKE);
		iPaint.setStrokeWidth(2);
		iPaint.setDither(true);
		iPaint.setAntiAlias(true);
		Enemy aEnemy=new Enemy(15,iDrawView.iPosList,0,iPaint,30,20,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight);
		iDrawView.enemies.add(aEnemy);	
		
		try {
			ResBitmaps.InitResBitmaps(this.getResources(),iPass,iWidth/18,iHeight/10);
			//ResBitmaps.getResBitmaps().Resize(iWidth/18, iHeight/10);
		} catch (Exception e) {
			// TODO: handle exception
			Toast.makeText(this, e.getMessage(), 2000).show();
			Log.i("except",e.getMessage());
		}
		
		
		
		
		
		iDrawView.iTowerLabels.add(new TowerLabel("a",40,40,Color.BLUE,0,20,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		iDrawView.iTowerLabels.add(new TowerLabel("a",200,40,Color.BLUE,2,80,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		iDrawView.iTowerLabels.add(new TowerLabel("b",120,40,Color.GREEN,1,40,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		
		iDrawView.iTowerLabels.add(new TowerLabel("b",280,40,Color.GREEN,3,160,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		iDrawView.iTowerLabels.add(new TowerLabel("a",360,40,Color.BLUE,4,320,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		
		StartGame();
	}
	
	 void GetRoad() {
		// TODO Auto-generated method stub
		iDrawView.iRoadBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.road);
	}

	Bitmap ReSize(Bitmap aBitmap){
		 float scaleWidth = ((float) iWidth) / aBitmap.getWidth();
		  float scaleHeight = ((float) iHeight) / aBitmap.getHeight();
		  Matrix matrix = new Matrix();
		  matrix.postScale(scaleWidth, scaleHeight);
		  return Bitmap.createBitmap(aBitmap, 0, 0, aBitmap.getWidth(), aBitmap.getHeight(), matrix, true);
	}
	
	void GetBackGround()
	{
		switch (iPass) {
		case 1:
			    // 取得想要缩放的matrix参数
			    // 得到新的图片
			iDrawView.iBackGroundBitmap= ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg1));
			break;
		case 2:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg2));
			
			break;
		case 3:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg3));
			break;
		case 4:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg4));
			break;
		case 5:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg5));
			break;
		case 6:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg6));
			break;
		case 7:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg7));
			break;
		case 8:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg8));
			break;
		case 9:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg9));
			break;

		default:
			iDrawView.iBackGroundBitmap=ReSize(BitmapFactory.decodeResource(getResources(), R.drawable.bg9));
			break;
			
		}
		
		iDrawView.iGoldBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.gold);
		iDrawView.iHpBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.heart);
		iDrawView.iScoreBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.star);
	}


	void StartGame()
	{
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				iHandler.sendEmptyMessage(1);
			}
		}, 100);
	}

	//敌人的这个需要大改
	void UpdateEnemy()
	{
		//确定新敌人出现的时间，每次间隔3分钟发一波，每次出20只怪兽
		
		//这里就可以确定总的时间了
		if(iTimeTicker<=3*fps)
		{
			iDrawView.onStartBoolean=true;
		}
		else if(iTimeTicker>5*fps&&iTimeTicker<=25*fps) {
			if(iTimeTicker%25==0)
			{
				iDrawView.enemies.add(new Enemy(3+iPass,iDrawView.iPosList, 0, iPaint, 3*iPass, 30+50*iPass,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
			}
			
		}
		else if(iTimeTicker>40*fps&&iTimeTicker<=70*fps)
		{
			if(iTimeTicker%20==0)
			{
				iDrawView.enemies.add(new Enemy(4+iPass,iDrawView.iPosList, 1, iPaint, 4*iPass, 50+70*iPass,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
			}
		}
		else if(iTimeTicker>85*fps&&iTimeTicker<=125*fps)
		{
			if(iTimeTicker%15==0)
			{
				iDrawView.enemies.add(new Enemy(5+iPass,iDrawView.iPosList, 2, iPaint, 5*iPass, 60+90*iPass,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
				
			}
		}
		else if(iTimeTicker>140*fps&&iTimeTicker<=175*fps)
		{
			if(iTimeTicker%12==0)
			{
				iDrawView.enemies.add(new Enemy(6+iPass,iDrawView.iPosList, 3, iPaint, 6*iPass, 70+130*iPass,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
				
			}
		}
		else if(iTimeTicker==190*fps)
		{
			iDrawView.enemies.add(new Enemy(6+iPass,iDrawView.iPosList, 4, iPaint, 8*iPass, 200+200*iPass,iDrawView.iSpriteWidth,iDrawView.iSpriteHeight));
		}
		
		
		if(iTimeTicker>200*fps &&iDrawView.enemies.size()==0)
		{
			iDrawView.GameState=iDrawView.WIN;
		}
		
		
		
		
		int tindex=iDrawView.enemies.size()-1;
		Enemy enemy;
		for(;tindex>=0;tindex--)
		{
			
			enemy=iDrawView.enemies.get(tindex);
			Log.i("enHP","当前Hp:"+String.valueOf(enemy.iHp));
			//如果Hp为0，则取消这个怪兽
			if(enemy.iHp==0||enemy.iHp<0)
			{
				iDrawView.iGold+=10;
				iDrawView.iScore+=1;
				iDrawView.enemies.remove(enemy);
				//另外，还需对其加分
				
			}
		}
		
		
		
		int index=iDrawView.enemies.size()-1;
		
		Enemy emEnemy;
		
		
		
		for(;index>=0;index--)
		{
			toLog(index);
			try {
				emEnemy=iDrawView.enemies.get(index);
				if (emEnemy.iPathIndex==emEnemy.iPosList.size()-1) //
				{
					//消失，扣分
					iDrawView.iHP-=1;
					iDrawView.enemies.remove(emEnemy);
				}
				else 
				{
					//进行移动,由于只有水平和垂直速度
					//先判断是否已经接近了
					int DestXL=Math.abs(emEnemy.iX-emEnemy.iPosList.get(emEnemy.iPathIndex+1).iX);
					int DestYL=Math.abs(emEnemy.iY-emEnemy.iPosList.get(emEnemy.iPathIndex+1).iY);
					Log.i("pos", "DestXL:"+String.valueOf(DestXL)+",DestYL:"+String.valueOf(DestYL));
					
					if(DestXL<emEnemy.iSpeed&&DestYL<emEnemy.iSpeed)
					{
						
						Log.i("nextPos","下一个节点:"+String.valueOf(emEnemy.iX)+","+String.valueOf(emEnemy.iY));
						emEnemy.iX=emEnemy.iPosList.get(emEnemy.iPathIndex+1).iX;
						emEnemy.iY=emEnemy.iPosList.get(emEnemy.iPathIndex+1).iY;
						emEnemy.iPathIndex++;
					}
					
					else if((emEnemy.iPosList.get(emEnemy.iPathIndex).iX-emEnemy.iPosList.get(emEnemy.iPathIndex+1).iX)==0)
					{
						//垂直
						if(emEnemy.iPosList.get(emEnemy.iPathIndex+1).iY-emEnemy.iPosList.get(emEnemy.iPathIndex).iY>0)
						{
							//up
							emEnemy.iY+=emEnemy.iSpeed;
						}
						else if(emEnemy.iPosList.get(emEnemy.iPathIndex+1).iY-emEnemy.iPosList.get(emEnemy.iPathIndex).iY<0)
						{
							//down
							emEnemy.iY-=emEnemy.iSpeed;
						}
						
					}
					else if((emEnemy.iPosList.get(emEnemy.iPathIndex).iY-emEnemy.iPosList.get(emEnemy.iPathIndex+1).iY)==0)
					{
						//水平
						if(emEnemy.iPosList.get(emEnemy.iPathIndex+1).iX-emEnemy.iPosList.get(emEnemy.iPathIndex).iX>0)
						{
							//up
							emEnemy.iX+=emEnemy.iSpeed;
						}
						else if(emEnemy.iPosList.get(emEnemy.iPathIndex+1).iX-emEnemy.iPosList.get(emEnemy.iPathIndex).iX<0)
						{
							//down
							emEnemy.iX-=emEnemy.iSpeed;
						}
					}
					
					//if turn
					//next point
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.i("light", e.getMessage());
			}
			
		}
		
	}
	void UpdateBg()
	{
		
	}
	
	void UpdateMain()
	{
		iTimeTicker++;
		// 这里更新所有的内容
		//更新所有的敌人
		
		UpdateLogic();
		
		UpdateBg();//更新背景
		UpdateBullet();
		UpdateEnemy();
		UpdateOfferTowerList();
		UpdateTower();
		
		
		UpdateEnd();
		
		
		if(iDrawView.GameState==iDrawView.ON)
		{
			StartGame();
		}
		else if(iDrawView.GameState==iDrawView.WIN)
		{
			WinGame();
		}
		
		
		iDrawView.invalidate();
	}
		
	void WinGame()
	{
		GlobalObject tmpGlobalObject=(GlobalObject) getApplication();
		if(tmpGlobalObject.iPass<9&&iPass==tmpGlobalObject.iPass)
		{
			tmpGlobalObject.iPass+=1;
			
		}
		tmpGlobalObject.iGold+=100;
		
		ShowEndGame("胜利！！你已经击败了 "+String.valueOf(globalObject.iScore)+"只神兽!!!!");
	}
	
	void UpdateEnd()
	{
		if(iDrawView.iHP==0)
		{
			iDrawView.GameState=iDrawView.END;
			ShowEndGame("你击败了 "+String.valueOf(globalObject.iScore)+"只神兽!!!!");	
		}
	}
	void UpdateLogic()
	{
		if(iTimeTicker%20==0)
		{
			iDrawView.iGold+=1;
		}
	}
	
	public void UpdateBullet()
	{
		Bullet tmpBullet;
		int index=iDrawView.iBullets.size()-1;
		
		for(;index>=0;index--)
		{
			tmpBullet=iDrawView.iBullets.get(index);
			if(tmpBullet.ToProg()==1)
			{
				iDrawView.iBullets.remove(tmpBullet);
			}
		}
	}
	
	
	public void UpdateTower() {
		// TODO Auto-generated method stub
		int index=iDrawView.towers.size()-1;
		for(;index>=0;index--)
		{
			//判断是否有敌人到达
			iDrawView.towers.get(index).iSendBullet();
		}
	}

	//更新允许更新的塔的列表
	void UpdateOfferTowerList()
	{
		
		
	}
	void CreatePath()
	{
		int[] X={0,400,400,700};
		int [] Y={200,200,400,400};
		//假设0.1描更新一次
		int xOffset=16;
		int yOffset=16;
		iDrawView.iPosList=new ArrayList<CPos>();
		int [][] Xindex={
				{0,5,5,29,29,10,10,18,18,35},
				{0,8,8,2,2,11,11,13,13,22,22,23,23,28,28,35},
				{0,7,7,5,17,17,10,10,6,6,25,25,35},
				{0,35,35,25,25,3,3,1,1,19,19,35},
				{0,10,10,3,3,25,25,23,23,15,15,14,14,19,19,35},
				{0,33,33,22,22,17,17,6,6,1,1,32,32,35},
				{0,18,18,4,4,28,28,18,18,28,28,35},
				{0,3,3,10,10,7,7,16,16,28,28,16,16,28,28,35},
				{0,33,33,1,1,5,5,35}
		
		};
		int [][] Yindex={
				{3,3,17,17,3,3,12,12,9,9},
				{4,4,9,9,16,16,13,13,6,6,11,11,15,15,4,4},
				{4,4,7,7,7,11,11,12,12,15,15,5,5},
				{4,4,9,9,8,8,9,9,13,13,14,14},
				{5,5,8,8,18,18,9,9,5,5,9,9,13,13,12,12},
				{4,4,12,12,8,8,15,15,7,7,18,18,13,13},
				{5,5,14,14,5,5,12,12,5,5,12,12},
				{5,5,18,18,14,14,7,7,4,4,15,15,4,4,15,15},
				{6,6,10,10,16,16,14,14}
		
		};
		
		GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
		int tindex=0;
		for(;tindex<Xindex[iPass-1].length;tindex++)
		{iDrawView.iPosList.add(new CPos(Xindex[iPass-1][tindex]*tmpGlobalObject.iSreenWidth/36, Yindex[iPass-1][tindex]*tmpGlobalObject.iSreenHeight/20));
			
		}

	
			
	}
	
	
	void CreatePathPoint()
	{
		int index=0;
		GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
		
		//Toast.makeText(getApplicationContext(), String.valueOf(tmpGlobalObject.iSreenWidth), Toast.LENGTH_LONG).show();
		int deltax=tmpGlobalObject.iSreenWidth/18; //这两个只是表示 
		int deltay=tmpGlobalObject.iSreenHeight/18;
		
		
		
		int srcx=0;
		int srcy=0;
		int destx=0;
		int desty=0;
		for (index=0;index<iDrawView.iPosList.size()-1;index++)
		{
			srcx=iDrawView.iPosList.get(index).iX;
			srcy=iDrawView.iPosList.get(index).iY;
			destx=iDrawView.iPosList.get(index+1).iX;
			desty=iDrawView.iPosList.get(index+1).iY;
			
			if(srcx==destx)
			{
				//delta=48;
				int num =Math.abs(desty-srcy)/deltay;
				int abs=desty-srcy;
				for (int aindex=0;aindex<=num;aindex++)
				{
					if(abs>=0)
					{
						iDrawView.iCPathPoints.add(new CPathPoint(srcx,srcy+deltay*aindex));
					}
					
					else 
					{
						iDrawView.iCPathPoints.add(new CPathPoint(srcx,srcy-deltay*aindex));
					}
				}
			}
			else if(srcy==desty)
			{
				//delta=48;
				int num=Math.abs(destx-srcx)/deltax;
				int abs=destx-srcx;
				for(int aindex=0;aindex<=num;aindex++)
				{
					if (abs>0)
					{
						iDrawView.iCPathPoints.add(new CPathPoint(srcx+deltax*aindex, srcy));
					}
					else 
					{
						iDrawView.iCPathPoints.add(new CPathPoint(srcx-deltax*aindex, srcy));
					}
				}
			}
		}
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		//iStart=false;
		try {
			mPlayer.stop();
			ResBitmaps.getResBitmaps().Recy();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
		
		super.onDestroy();
	}

	void toLog(int aX)
	{
		//Log.i("light", String.valueOf(aX));
	}
	
	//开始考虑暂停和结束了
	//那么就有三种询问，重来，返回主菜单，以及退出
	
	
	void ShowEndGame(String aTitle)
	{
		new AlertDialog.Builder(this).setTitle(aTitle).setPositiveButton("重来", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Init();
			}
		}).setNeutralButton("返回主菜单 ", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(OneActivity.this,TwoActivity.class);
				startActivity(intent);
				finish();
			}
		}).setNegativeButton("退出", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
				tmpGlobalObject.iHp=iDrawView.iHP;
				tmpGlobalObject.iGold=iDrawView.iGold;
				tmpGlobalObject.iScore=iDrawView.iScore;
				
				editor.putInt("Hp", tmpGlobalObject.iHp);
				editor.putInt("Gold", tmpGlobalObject.iGold);
				editor.putInt("Pass",tmpGlobalObject.iPass);
				editor.commit();
				finish();
				System.exit(0);
			}
		}).show();
	}

	

	
	void ShowPause()
	{
		
		
		iBuilder=new AlertDialog.Builder(this);
		iBuilder.setIcon(R.drawable.tower02).setPositiveButton("继续", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				ReSumeGame();
			}
		}).setNeutralButton("返回主菜单", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(OneActivity.this,TwoActivity.class);
				startActivity(intent);
				finish();
				
			}
		}).setNegativeButton("退出", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		}).setTitle("暂停");
		iBuilder.show();
		
		
		
	}

	
	void ReSumeGame()
	{
		iDrawView.GameState=iDrawView.ON;
		UpdateMain();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK&&event.getRepeatCount()==0)
		{
			
			iDrawView.GameState=iDrawView.PAUSE;
			ShowPause();
			return false;
		}
		
		return false;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		iDrawView.GameState=iDrawView.PAUSE;
		GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
		tmpGlobalObject.iHp=iDrawView.iHP;
		tmpGlobalObject.iGold=iDrawView.iGold;
		tmpGlobalObject.iScore=iDrawView.iScore;
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		
		
		if(iDrawView.GameState==iDrawView.PAUSE)
		{
			ShowPause();
		}
		super.onResume();
	}

	
	
	
	
	
	




	
	
	public void WriteObj(Object aSaveObj)
	{
		try {
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				File sdcardFile=Environment.getExternalStorageDirectory();
				File folderFile=new File(sdcardFile+FOLDER_PATH);
				if(!folderFile.exists())
				{
					folderFile.mkdirs();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("logo",e.getMessage() );
			e.printStackTrace();
		}
		
		
		
		try {
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				//Log.i("logo","正在保存ip地址和端口" );
				File sdcard=Environment.getExternalStorageDirectory();
				File tar=new File(sdcard+FILE_PATH);
				FileOutputStream fStream=new FileOutputStream(tar);
				ObjectOutputStream fosStream=new ObjectOutputStream(fStream);
				fosStream.writeObject(aSaveObj);
				fosStream.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("logo","写入失败");
			//Log.i("logo", e.getMessage());
			e.printStackTrace();
		}
	}
	
	public SaveObj ReadObj()
	{
		
		try {
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				File sdcardFile=Environment.getExternalStorageDirectory();
				File folderFile=new File(sdcardFile+FOLDER_PATH);
				if(!folderFile.exists())
				{
					folderFile.mkdirs();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("logo",e.getMessage() );
			e.printStackTrace();
		}
		
		try {
			if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
			{
				//Log.i("logo","正在读取ip地址和端口");
				File sdcard=Environment.getExternalStorageDirectory();
				File tarFile=new File(sdcard+FILE_PATH);
				FileInputStream fStream=new FileInputStream(tarFile);
				ObjectInputStream fiStream=new ObjectInputStream(fStream);
				SaveObj fuc=(SaveObj)fiStream.readObject();
				fiStream.close();
				return fuc;
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("logo",e.getLocalizedMessage());
			e.printStackTrace();
		}
    	return null;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
		tmpGlobalObject.iHp=iDrawView.iHP;
		tmpGlobalObject.iGold=iDrawView.iGold;
		tmpGlobalObject.iScore=iDrawView.iScore;
		tmpGlobalObject.enemies=iDrawView.enemies;
		tmpGlobalObject.towers=iDrawView.towers;
		tmpGlobalObject.bullets=iDrawView.iBullets;
		
		super.onSaveInstanceState(outState);
		
		
		
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		GlobalObject tmpGlobalObject =(GlobalObject)getApplication();
		iDrawView.iHP=tmpGlobalObject.iHp;
		iDrawView.iGold=tmpGlobalObject.iGold;
		iDrawView.iScore=tmpGlobalObject.iScore;
		iDrawView.enemies=tmpGlobalObject.enemies;
		iDrawView.towers=tmpGlobalObject.towers;
		iDrawView.iBullets=tmpGlobalObject.bullets;
		super.onRestoreInstanceState(savedInstanceState);
		
	}
	

	
}
