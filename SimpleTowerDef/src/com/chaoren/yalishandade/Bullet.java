package com.chaoren.yalishandade;

import java.io.Serializable;

import android.R.integer;
import android.graphics.Canvas;
import android.util.Log;

public class Bullet extends Sprite implements Serializable{
	public  static final int NORMAL=0;
	public static final int LONG=2;
	
	public static final int SLOW=1;
	public static final int BAOJI=3;
	public static final int AREA=4;
	public static final int ENDTIME=5;//由于10秒消失，所以表示消失的时间为1秒
	
	public int iSpeed=60;// 子弹的速度
	
	
	
	int fps=10;//帧数
	public int iHurt;
	public int iType;
	public Enemy iEnemy;
	double GAOJIGAILV=0.5;
	Boolean isBaoJI=false;
	public int iLevel;
	
	public int iBulletTimerStep=0;
	//初始化函数中，当中的坐标是塔的坐标，但是这里的话，好像不是
	
	public Bullet(int aType,Enemy aEnemy,int aX,int aY,int aLevel)
	{
		iX=aX;
		iY=aY;
		iLevel=aLevel;
		iEnemy=aEnemy;
				
		//根据类型进行选择效果
		iType=aType;
		switch (iType) {
		case NORMAL:
			iHurt=5+5*iLevel;
			break;
		case LONG:
			iHurt=3+iLevel;
			break;
		case AREA:
			iHurt=50*iLevel;
			break;
		case SLOW:
			iHurt=5+5*iLevel;
			break;
		case BAOJI:
			iHurt=30+10*iLevel;
			break;
		default:
			break;
		}
	}
	
	//子弹发送流程
	/**
	 * 1 发射之后，判断是否消亡
	 * 2 如果没有消亡，判断是否碰撞
	 * 3 如果碰撞，那么启动效果
	 * 4 如果已经消亡，判断减速效果
	 * 5 
	 */
	
	//在iOverTime=-1发生效果的东西: 正常，暴击，减速，范围
	//在iOverTime=0 发生的效果 减速：恢复原来速度
	//在iOverTime>0 时发生的效果：持续伤害
	
	
	public int ToProg()
	{
		if(iOverTime==-1)
		{
			if(DetectCrash(iEnemy))
			{
				//发生碰撞了
				StartEffect();
				return 0;
			}
			else  //没有发生碰撞，准备进行应该是这里的问题
			{
				
				
				int desty=(int) (iY+iSpeed*(iEnemy.iY-iY)/Math.sqrt(((iEnemy.iX-iX)*(iEnemy.iX-iX)+(iEnemy.iY-iY)*(iEnemy.iY-iY))));
				int destx=(int) (iX+iSpeed*(iEnemy.iX-iX)/Math.sqrt(((iEnemy.iX-iX)*(iEnemy.iX-iX)+(iEnemy.iY-iY)*(iEnemy.iY-iY))));
				
				if(desty==0&&destx==0)
				{
					Log.i("XYZ","SIN:"+String.valueOf((iEnemy.iY-iY)/Math.sqrt(((iEnemy.iX-iX)*(iEnemy.iX-iX)+(iEnemy.iY-iY)*(iEnemy.iY-iY)))));
					Log.i("XYZ","COS:"+String.valueOf((iEnemy.iX-iX)/Math.sqrt(((iEnemy.iX-iX)*(iEnemy.iX-iX)+(iEnemy.iY-iY)*(iEnemy.iY-iY)))));
					Log.i("XYZ", "原坐标:"+String.valueOf(iX)+","+String.valueOf(iY));
					Log.i("XYZ", "敌人坐标"+String.valueOf(iEnemy.iX)+","+String.valueOf(iEnemy.iY));
				}
				Log.i("bulletDest", "新坐标:"+String.valueOf(destx)+","+String.valueOf(desty));
				
				
				
				
				iX=destx;
				iY=desty;	
			}
		}
		else if(iOverTime==0)
		{
			//同时恢复敌人的状态
			//消失，准备要取消自己了
			//因为自己不能够取消自己，所以让这个对象列表取消自己就可以了
			switch (iType) {
			case SLOW:
				iEnemy.iSpeed=iEnemy.iSpeed*iLevel;
				break;

			default:
				break;
			}
			
			return 1; // 将这个对象撤销
		}
		else if(iOverTime>0)
		{
			iX=iEnemy.iX;
			iY=iEnemy.iY;
			OnEffect();
			iOverTime--;
			
			return 2;
		}
		return 0;
	}
	
	public void OnEffect()
	{
		switch (iType) {
		case LONG:
			Dameage();
			break;
		default:
			break;
		}
	}
	
	public void StartEffect()
	{
		
		//根据类型出现效果
		switch (iType) {
		//普通攻击
		case NORMAL:
			iOverTime=ENDTIME;
			Dameage();
			break;
		
		//持续伤害
		case LONG:
			iOverTime=2*iLevel*ENDTIME;
			break;
			
		case SLOW:
			iOverTime=3*ENDTIME;
			if(iEnemy.iSpeed/(iLevel+1)==0)
			{
				iEnemy.iSpeed=1;
			}
			else 
			{
				iEnemy.iSpeed=iEnemy.iSpeed/(iLevel+1);
			}
			break;
		
		case BAOJI:
			double gailv=Math.random();
			if (gailv>GAOJIGAILV)
			{	
				iHurt=iHurt*(iLevel+1);
				isBaoJI=true;
			}
			Dameage();
			
		case AREA:
			iOverTime=ENDTIME;
			Dameage();
			break;
			
			
		default:
			break;
		}
	}
	
	
	public void Dameage()
	{
		//Log.i("EnemyHP:", String.valueOf(iEnemy.iHp));
		if(iEnemy.iHp<iHurt)
		{
			iEnemy.iHp=0;
		}
		else 
		{
			iEnemy.iHp-=iHurt;
		}
	}
	
	
	public void Draw(Canvas canvas)
	{
		//判断类型，然后根据类型获取图片，
		//Log.i("bullet", "子弹画画");
		iBulletTimerStep++;
		if(iOverTime==-1)
		{
			canvas.drawBitmap(ResBitmaps.getResBitmaps().iBulletBitmaps.get(iType).norBitmaps.get(iBulletTimerStep%1), iX,iY,null);
			
		}
		else 
		{
			//canvas.drawBitmap(ResBitmaps.getResBitmaps().iBulletBitmaps.get(iType).effectBitmaps.get(iBulletTimerStep%1), iX,iY,null);
		}
		
		
	}
	
	
	
}
